package com.steeplesoft.micronaut.model

import io.micronaut.security.authentication.providers.UserState
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "users")
data class User(@Id
                @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
                @SequenceGenerator(name="user_generator", sequenceName = "user_seq")
                var id: Long? = null,
                val email : String? = null, val pass : String? = null) : UserState {

    override fun getUsername(): String {
        return email!!
    }

    override fun getPassword(): String {
        return pass!!
    }

    override fun isPasswordExpired(): Boolean {
        return false
    }

    override fun isAccountExpired(): Boolean {
        return false
    }

    override fun isAccountLocked(): Boolean {
        return false
    }

    override fun isEnabled(): Boolean {
        return true
    }
}