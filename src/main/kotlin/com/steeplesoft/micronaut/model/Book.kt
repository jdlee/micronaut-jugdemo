package com.steeplesoft.micronaut.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "books")
data class Book(@Id
                @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_generator")
                @SequenceGenerator(name="book_generator", sequenceName = "book_seq")
                var id: Long? = null,var title : String)
//                var authors: MutableList<Author> = mutableListOf()) {
