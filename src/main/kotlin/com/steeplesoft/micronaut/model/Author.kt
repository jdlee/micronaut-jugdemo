package com.steeplesoft.micronaut.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "authors")
data class Author(@Id
                  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_generator")
                  @SequenceGenerator(name="author_generator", sequenceName = "author_seq")
                  var id: Long? = null,
                  var name : String? = null)