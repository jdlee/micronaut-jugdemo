package com.steeplesoft.micronaut.service

import com.steeplesoft.micronaut.model.Author
import com.steeplesoft.micronaut.model.User
import io.micronaut.aop.Around
import io.micronaut.spring.tx.annotation.Transactional
import java.util.Optional
import javax.inject.Singleton
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.validation.constraints.NotNull

@Singleton
@Around
class AuthorService(@PersistenceContext val entityManager: EntityManager ) {
    @Transactional(readOnly = true)
    fun findById(id: Long): Author? {
        return entityManager.find(Author::class.java, id)
    }

    @Transactional
    fun addAuthor(author: Author) : Author {
        entityManager.persist(author)
        return author
    }

    @Transactional(readOnly = true)
    fun findAuthorByName(name : String) : Author? {
        val result = entityManager.createQuery("SELECT a FROM Author a WHERE a.name = :name", Author::class.java)
                .setParameter("name", name)
                .resultList
        return if (result.size > 0) result[0] else null
    }

    @Transactional
    fun updateAuthor(author : Author) : Author {
        return entityManager.merge(author)
    }

    @Transactional(readOnly = true)
    fun getAuthors(): List<Author> {
        return entityManager.createQuery("SELECT a FROM Author a ORDER BY a.name", Author::class.java).resultList
    }

    @Transactional
    fun delete(@NotNull id: Long) : Boolean {
        val author = findById(id)
        return if (author != null) {
            entityManager.remove(author)
            true
        } else {
            false
        }
    }
}