package com.steeplesoft.micronaut.service

import com.steeplesoft.micronaut.model.User
import com.steeplesoft.micronaut.security.DemoPasswordEncoder
import io.micronaut.aop.Around
import io.micronaut.spring.tx.annotation.Transactional
import javax.inject.Singleton
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Singleton
@Around
class UserService(@PersistenceContext val entityManager: EntityManager,
                  private val passwordEncoder: DemoPasswordEncoder) {
    @Transactional(readOnly = true)
    fun findUser(email : String) : User? {
        return try {
            entityManager.createQuery("SELECT u FROM User u WHERE email = :email", User::class.java)
                    .setParameter("email", email)
                    .singleResult
        } catch (e : Exception) {
            println(e.localizedMessage)
            null
        }
    }

    @Transactional
    fun addUser(userName : String, password : String) : User {
        val user = User(email = userName, pass = passwordEncoder.encode(password))
        entityManager.persist(user)
        return user
    }
}