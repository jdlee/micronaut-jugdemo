package com.steeplesoft.micronaut

import io.micronaut.runtime.Micronaut

object Application {
    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("com.steeplesoft.micronaut")
                .mainClass(Application.javaClass)
                .start()
    }
}