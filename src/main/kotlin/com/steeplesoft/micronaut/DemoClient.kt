package com.steeplesoft.micronaut

import com.steeplesoft.micronaut.model.Author
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Header
import io.micronaut.http.annotation.Post
import io.micronaut.http.client.annotation.Client
import io.micronaut.security.authentication.UsernamePasswordCredentials
import io.micronaut.security.token.jwt.render.BearerAccessRefreshToken

@Client("/")
interface DemoClient {
    @Post("login")
    fun login(@Body creds : UsernamePasswordCredentials) : BearerAccessRefreshToken

    @Get("/authors/index")
    fun getIndex(@Header authorization : String?) : String

    @Get("/authors")
    fun getAuthors(@Header authorization : String?) : List<Author>

    @Get("/authors/{id}")
    fun getAuthor(@Header authorization : String?, id : Long) : Author?

    @Post("/authors")
    fun createAuthor(@Header authorization: String?, @Body author: Author) : Author

    @Post("/authors/{id}")
    fun updateAuthor(@Header authorization: String?, id : Long, @Body author: Author): Author

    @Delete("/authors/{id}")
    fun deleteAuthor(@Header authorization : String?, id : Long) : HttpStatus
}