package com.steeplesoft.micronaut.controller

import com.steeplesoft.micronaut.model.Author
import com.steeplesoft.micronaut.service.AuthorService
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.exceptions.HttpStatusException
import io.micronaut.security.annotation.Secured

@Secured("isAuthenticated()")
@Controller("/authors")
class AuthorController(private val authorService: AuthorService) {

    @Get("/index")
    fun index(): HttpResponse<String> {
        return HttpResponse.ok("author")
    }

    @Get
    fun getAuthors(): List<Author> {
        return authorService.getAuthors();
    }

    @Get("{id}")
    fun getAuthor(id: Long): Author? {
        return authorService.findById(id)
    }

    @Post("{id}")
    fun updateAuthor(id: Long, author: Author): Author {
        if (id != author.id) {
            throw HttpStatusException(HttpStatus.BAD_REQUEST,
                    "The author passed does not match the author specified (you can not change the Author id")
        }
        return authorService.updateAuthor(author)
    }

    @Post
    fun addAuthor(author: Author): Author {
        return authorService.addAuthor(author)
    }

    @Delete("{id}")
    fun deleteAuthor(id: Long): HttpStatus {
        return if (authorService.delete(id)) HttpStatus.OK else HttpStatus.NOT_FOUND
    }
}