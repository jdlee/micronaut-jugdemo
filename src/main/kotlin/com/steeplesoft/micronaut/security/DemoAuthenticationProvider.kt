package com.steeplesoft.micronaut.security

import com.steeplesoft.micronaut.service.UserService
import io.micronaut.security.authentication.AuthenticationFailed
import io.micronaut.security.authentication.AuthenticationProvider
import io.micronaut.security.authentication.AuthenticationRequest
import io.micronaut.security.authentication.AuthenticationResponse
import io.micronaut.security.authentication.UserDetails
import io.reactivex.Flowable
import org.reactivestreams.Publisher
import java.util.ArrayList
import javax.inject.Singleton


@Singleton
class DemoAuthenticationProvider(private val userService: UserService,
                                 private val passwordEncoder: DemoPasswordEncoder) : AuthenticationProvider {
    override fun authenticate(authenticationRequest: AuthenticationRequest<*, *>): Publisher<AuthenticationResponse> {
        val user = userService.findUser(authenticationRequest.identity as String)
        return (if (user != null && passwordEncoder.matches(authenticationRequest.secret as String, user.password)) {
            Flowable.just(UserDetails(user.email, ArrayList()))
        } else {
            Flowable.just(AuthenticationFailed())
        })
    }
}