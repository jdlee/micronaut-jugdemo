package com.steeplesoft.micronaut

import com.steeplesoft.micronaut.model.Author
import com.steeplesoft.micronaut.service.AuthorService
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class AuthorServiceTest {
    @Inject
    lateinit var authorService: AuthorService

    @Test
    fun createAuthor() {
        val author = authorService.addAuthor(Author(null, "C.S. Lewis"))
        Assertions.assertNotNull(author.id)
    }
}