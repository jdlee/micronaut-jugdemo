package com.steeplesoft.micronaut

import com.steeplesoft.micronaut.model.Author
import com.steeplesoft.micronaut.service.AuthorService
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxStreamingHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.security.authentication.UsernamePasswordCredentials
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class AuthorClientTest {
    private val testAuthors = listOf("Tom Clancy", "Brandon Sanderson")

    @Inject
    lateinit var authorService: AuthorService

    @Inject
    @field:Client("/")
    lateinit var client: DemoClient

    var authz : String? = null

    @BeforeEach
    fun setup() {
        val token = client.login(UsernamePasswordCredentials(AuthorControllerTest.USERNAME, AuthorControllerTest.PASSWORD))
        authz = "Bearer ${token.accessToken}"

        testAuthors.forEach {
            if (authorService.findAuthorByName(it) == null) {
                authorService.addAuthor(Author(name = it))
            }

        }
    }

    @Test
    fun clientShouldNotBeNull() {
        Assertions.assertNotNull(client);
    }

    @Test
    fun getIndex() {
        Assertions.assertEquals("author",client.getIndex(authz!!))
    }

    @Test
    fun createAuthor() {
        val author = Author(name = "John Grisham")
        val newAuthor = client.createAuthor(authz, author)
        Assertions.assertEquals(author.name, newAuthor.name)
    }

    @Test
    fun getAuthor() {
        val authors =  client.getAuthors(authz)
        val author = authors[0]
        val id = author.id!!
        val author2 = client.getAuthor(authz, id)
        Assertions.assertEquals(author, author2)
    }

    @Test
    fun authorList() {
        Assertions.assertEquals(testAuthors.size, client.getAuthors(authz).size)
    }

    @Test
    fun updateAuthor() {
        val authors =  client.getAuthors(authz)
        val author = authors[0]
        author.name = "Updated"
        val author2 = client.updateAuthor(authz, author.id!!, author)
        Assertions.assertEquals("Updated", author2.name)
        val author3 = client.getAuthor(authz, author.id!!)
        Assertions.assertEquals(author, author3)
    }

    @Test
    fun deletingKnownAuthorReturnsOK() {
        val id = client.getAuthors(authz)[0].id!!
        val response = client.deleteAuthor(authz, id)
        Assertions.assertEquals(HttpStatus.OK, response)
    }

    @Test
    fun deletingUnknownAuthorReturnsNotFound() {
//        Assertions.assertEquals(HttpStatus.NOT_FOUND, client.deleteAuthor(authz, 99999))
    }
}
