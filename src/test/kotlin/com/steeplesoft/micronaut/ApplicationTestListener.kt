package com.steeplesoft.micronaut

import com.steeplesoft.micronaut.model.Author
import com.steeplesoft.micronaut.service.AuthorService
import com.steeplesoft.micronaut.service.UserService
import io.micronaut.context.event.ApplicationEventListener
import io.micronaut.runtime.server.event.ServerStartupEvent
import javax.inject.Singleton

@Singleton
class ApplicationTestListener(private val userServer: UserService) : ApplicationEventListener<ServerStartupEvent> {


    override fun onApplicationEvent(event: ServerStartupEvent?) {
        if (userServer.findUser(AuthorControllerTest.USERNAME) == null) {
            userServer.addUser(AuthorControllerTest.USERNAME, AuthorControllerTest.PASSWORD)
        }
    }
}