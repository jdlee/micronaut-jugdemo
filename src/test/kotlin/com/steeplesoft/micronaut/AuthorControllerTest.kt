package com.steeplesoft.micronaut

import com.steeplesoft.micronaut.model.Author
import com.steeplesoft.micronaut.service.AuthorService
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxStreamingHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.security.authentication.UsernamePasswordCredentials
import io.micronaut.security.token.jwt.render.BearerAccessRefreshToken
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class AuthorControllerTest {
    private val testAuthors = listOf("Tom Clancy", "Brandon Sanderson")

    @Inject
    @field:Client("/")
    lateinit var client: RxStreamingHttpClient

    @Inject
    lateinit var authorService: AuthorService

    @BeforeEach
    fun setup() {
        testAuthors.forEach {
            if (authorService.findAuthorByName(it) == null) {
                authorService.addAuthor(Author(name = it))
            }

        }
    }

    @Test
    fun testBadAuthentication() {
        Assertions.assertThrows(HttpClientResponseException::class.java) {
            val request = HttpRequest.POST("/login",
                    UsernamePasswordCredentials("bad", "user"))
            client.toBlocking().exchange(request, BearerAccessRefreshToken::class.java)
        }
    }

    @Test
    fun testGoodAuthentication() {
        login()
    }

    @Test
    fun testIndex() {
        val request = HttpRequest
                .GET<String>("/authors/index")
                .header(HttpHeaders.AUTHORIZATION, "Bearer ${login().accessToken}")
        Assertions.assertEquals("author",
                client.toBlocking().exchange(request, String::class.java).body())
    }

    @Test
    fun createAuthor() {
        val author = Author(name = "John Grisham")
        val request = HttpRequest
                .POST<Author>("/authors", author)
                .header(HttpHeaders.AUTHORIZATION, "Bearer ${login().accessToken}")
        Assertions.assertEquals(HttpStatus.OK,
                client.toBlocking().exchange(request, Author::class.java).status)
    }

    @Test
    fun getAuthor() {
        val authors =  getAuthors()
        val author = authors[0]
        val id = author.id
        val author2 = getAuthor(id!!)
        Assertions.assertEquals(author, author2)
    }

    @Test
    fun authorList() {
        Assertions.assertEquals(testAuthors.size, getAuthors().size)
    }

    @Test
    fun updateAuthor() {
        val authors =  getAuthors()
        val author = authors[0]
        author.name = "Updated"
        val author2 = client.toBlocking()
                .exchange(HttpRequest.POST<Author>("/authors/${author.id}", author)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer ${login().accessToken}"),
                        Author::class.java)
                .body()
        Assertions.assertEquals("Updated", author2?.name)
        val author3 = getAuthor(author.id!!)
        Assertions.assertEquals(author, author3)
    }

    @Test
    fun deletingKnownAuthorReturnsOK() {
        val id = getAuthors()[0].id
        val response = client.toBlocking()
                .exchange(HttpRequest.DELETE<Any>("/authors/$id")
                        .header(HttpHeaders.AUTHORIZATION, "Bearer ${login().accessToken}"),
                        String::class.java)
        Assertions.assertEquals(HttpStatus.OK, response.status)
    }

    @Test
    fun deletingUnknownAuthorReturnsNotFound() {
        Assertions.assertThrows(HttpClientResponseException::class.java) {
            client.toBlocking()
                    .exchange(HttpRequest.DELETE<Any>("/authors/9999999")
                            .header(HttpHeaders.AUTHORIZATION, "Bearer ${login().accessToken}"),
                            String::class.java)
        }
    }

    private fun getAuthor(id: Long): Author? {
        return client.toBlocking()
                .exchange(HttpRequest.GET<Author>("/authors/$id")
                        .header(HttpHeaders.AUTHORIZATION, "Bearer ${login().accessToken}"),
                        Author::class.java)
                .body()
    }

    private fun getAuthors(): List<Author> {
        return client.toBlocking()
                .exchange(HttpRequest.GET<List<Author>>("/authors")
                        .header(HttpHeaders.AUTHORIZATION, "Bearer ${login().accessToken}"),
                        Argument.of(
                                List::class.java,
                                Author::class.java)
                ).body() as List<Author>
    }

    private fun login(): BearerAccessRefreshToken {
        val request = HttpRequest.POST("/login", UsernamePasswordCredentials(USERNAME, PASSWORD))
        val response = client.toBlocking().exchange(request, BearerAccessRefreshToken::class.java)
        Assertions.assertEquals(HttpStatus.OK, response.status);

        return response.body()!!
    }

    companion object {
        val USERNAME = "jugdemo@steeplesoft.com"
        val PASSWORD = "password"
    }
}
